#include <stdio.h>
#include <stdlib.h>

int main()
{
    FILE *f = fopen("homelistings.csv", "r");
    FILE *s = fopen("small.txt", "w");
    FILE *m = fopen("med.txt", "w");
    FILE *l = fopen("large.txt", "w");
    if(!f)
    {
        printf("Couldn't open file\n");
        exit(1);
    }
    int zip, id, price, baths, beds, area;
    char address[30];
    while(fscanf(f,"%d,%d,%[^,],%d,%d,%d,%d", &zip,&id,address,&price,&beds,&baths,&area) != EOF)
    {
        if( area < 1000)
        {
            fprintf(s, " %s : %d\n", address, area);
        }
        else if( area > 2000)
        {
            fprintf(l, " %s : %d\n", address, area);
        }
        else
        {
            fprintf(m, " %s : %d\n", address, area);
        }
    }
    fclose(f);
    fclose(s);
    fclose(m);
    fclose(l);
    return 0;
}
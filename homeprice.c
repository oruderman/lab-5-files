#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>

int main()
{
    FILE *f;
    f = fopen("homelistings.csv", "r");
    if(!f)
    {
        printf("Couldn't open file\n");
        exit(1);
    }
    int zip, id, price, baths, beds, area;
    char address[30];
    int most =0;
    int min = 30000000;
    float count=0;
    float sum=0;
    float average;
    while(fscanf(f,"%d,%d,%[^,],%d,%d,%d,%d", &zip,&id,address,&price,&beds,&baths,&area) != EOF)
    {
        if(price>most)
        {
            most=price;
        }
        if(price<min)
        {
            min=price;
        }
        count++;
        sum+=price;
    }
   average = sum/count;
   printf("%d %d %f\n", min, most, average);
   fclose(f);
   return 0;
}
